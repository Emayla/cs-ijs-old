<main>
<div class="section_header">
  <h1><span>Publications</span></h1>
</div>
<div class="container">
  <div class="col-md-12">
    <h2><span>2015</span></h2>
    <ul class="publications">
      <li><a href="">Design of an Axial Flux Permanent Magnet Synchronous Machine Using Analytical Method and Evolutionary Optimization</a>, <em>IEEE T ENERGY CONVER</em></li>
      
      <li><a href="#publication-desc-2015" data-toggle="collapse">A measure for a balanced workload and its extremal values</a>, <em>Discrete Appl. Math.</em></li>
      <div id="publication-desc-2015" class="collapse publication">
        <!--<h3>A measure for a balanced workload and its extremal values</h3>-->
        <p>J. Govorčin, R. Škrekovski, V. Vukašinović, D. Vukičević <br />
        <em>Discrete Applied Mathematics</em>, 2015</p>
        <h4>abstract</h4>
        <p>In order to measure the extent to which the distribution of workload between actors in the network can be equalized, a degree-weighted measure for a balanced workload based on betweenness centrality is introduced. The goal of this study is to determine the extremal values of the introduced measure, as well as the graph structures where the extremal values are attained. Several real world networks were used for evaluation of the new invariant. The obtained results are used for statistical comparison with standard measures of centrality to demonstrate validity of the introduced measure.</p>
      </div>
      
      <li><a href="">Rooted level-disjoint partitions of Cartesian products</a>, <em>APPL MATH COMPUT</em></li>

      <li><a href="">Big-Data Analytics: a Critical Review and Some Future Directions</a>, <em>IJBIDM</em></li>

      <li><a href="">Comparison of a web-based dietary assessment tool with software for the evaluation of dietary records</a>, <em>ZV</em></li>

      <li><a href="">Using a Genetic Algorithm to Produce Slogans</a>, <em>Informatica</em></li>

      <li><a href="">Data Mining-Assisted Parameter Tuning of a Search Algorithm</a>, <em>Informatica</em></li>

      <li><a href="">A Case Analysis of Embryonic Data Mining Success</a>, <em>INT J INFORM MANAGE</em></li>
    </ul>
  </div>
</div>

<div class="container">
  <div class="col-md-12">
    <h2><span>2014</span></h2>
    <ul class="publications">
      <li><a href="">High correlation between paper- and web-based dietary records: a pilot study</a>, <em>Ann Nutr Metab</em></li>

      <li><a href="">Adult cancer patients on Home parenteral nutrition in Slovenia, data analysis in the Clinical Nutrition Unit in the Institute of Oncology in Ljubljana, in the period 2008–2012</a>, <em>ZV</em></li>

      <li><a href="">Estimation of sodium availability in food in Slovenia: results from household food purchase data from 2000 to 2009</a>, <em>ZV</em></li>

      <li><a href="#publication-desc-2014" data-toggle="collapse">Practical considerations in oscillation based test of SC biquad filters</a>, <em>INF TECHNOL CONTROL</em></li>

      <div id="publication-desc-2014" class="collapse publication">
        <!--<h3>Practical considerations in oscillation based test of SC biquad filters</h3>-->
        <p>Uroš Kač, Franc Novak <br />
        <em>Information technology and control</em>, 2014, Vol. 43, No. 1, pages: 28-36</p>
        <p><a href="mailto:">request for paper</a></p>
        <h4>abstract</h4>
        <p>Transformation of different types of switched-capacitor (SC) biquad filter stages based on Fleischer-Laker biquad SC structure in order to support oscillation based testing (OBT) have been proposed. The derived solutions are based on the generic Fleischer-Laker biquad SC structure assuming ideal characteristics of the employed components. In this paper, we explore the operation of the proposed OBT structures in the case of non ideal components. Furthermore, the efficiency of the proposed OBT schemes in terms of achieved fault coverage in comparison with other test methods is discussed.</p>
      </div>

      <li><a href="">Modeling acquaintance networks based on balance theory</a>, <em>Int. J. Appl. Math. Comp. Sci.</em></li>

      <li><a href="">A GRASS GIS parallel module for radio-propagation predictions</a>, <em>Int. J. Geogr. Inf. Sci.</em></li>

      <li><a href="">A GPU-based parallel-agent optimization approach for the service coverage problem in UMTS networks</a>, <em>CAI</em></li>
    </ul>
  </div>
</div>
</main>