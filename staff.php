<!-- Slider -->
<div class="header-banner"> 
  <script src="js/responsiveslides.min.js"></script> 
  <script>
       $(function () {
        $("#slider").responsiveSlides({
        auto: true,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        pager: true,
        });
       });
       </script>
  <div class="container">
    <div class="slider">
      <div class="callbacks_container">
        <div id="gallery">
          <ul class="rslides" id="slider">
            <li> <img src="images/pocket-size-kitchen-scale.png" alt="">
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<main>
<!-- Staff Section -->
<div class="section_header">
  <h1><span>Staff</span></h1>
</div>
<div class="container">
  <div class="row">
  <div class="col-xs-12 col-sm-6 col-md-3">
    <div class="person2">
      <img src="images/staff/anton_biasizzo.gif" class="img-responsive center-block" alt=" "> 
      <h3>Anton Biasizzo</h3>
      <p>Building A, Floor 1, Room 230 <br />
      +386 1 477 3297 <br />
      <a href="mailto:anton.biasizzo@ijs.si">anton.biasizzo@ijs.si</a> <br/>
      <a href="http://cs.ijs.si/biasizzo/biasizzo.html">http://cs.ijs.si/biasizzo/biasizzo.html</a>
      </p>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-md-3">
    <div class="person2">
      <img src="images/staff/jolanda_jakofcic.jpg" class="img-responsive center-block" alt=" "> 
      <h3>Jolanda Jakofčič (Secretary)</h3>
      <p>Building A, Floor 2, Room S8A <br />
      +386 1 477 3582 <br />
      <a href="mailto:jolanda.jakofcic@ijs.si">jolanda.jakofcic@ijs.si</a> <br />
      </p>
    </div>
  </div>
  <div class="clearfix visible-sm-block"></div>
  <div class="col-xs-12 col-sm-6 col-md-3">
    <div class="person2">
      <img src="images/staff/peter_korosec.jpg" class="img-responsive center-block" alt=" "> 
      <h3>Peter Korošec</h3>
      <p>Building A, Floor 2, Room S6F <br />
      +386 1 477 3460 <br />
      <a href="mailto:peter.korosec@ijs.si">peter.korosec@ijs.si</a> <br />
      <a href="http://cs.ijs.si/korosec/">http://cs.ijs.si/korosec/</a> <br />
      His areas of research include combinatorial/numerical optimization with modern metaheuristics and parallel/distributed computing.
      </p>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-md-3">
    <div class="person2">
      <img src="images/staff/barbara_korousic_seljak.jpg" class="img-responsive center-block" alt=" ">
      <h3>Barbara Koroušić Seljak</h3>
      <p>Building A, Floor 2, Room S6D <br />
      +386 1 477 3363 <br />
      <a href="mailto:barbara.korousic@ijs.si">barbara.korousic@ijs.si</a> <br />
      <a href="http://cs.ijs.si/korousic/korousic.html">http://cs.ijs.si/korousic/korousic.html</a>
      </p>
    </div>
  </div>
  </div>

  <div class="row">
  <div class="col-xs-12 col-sm-6 col-md-3">
    <div class="person2">
      <img src="images/staff/anton_biasizzo.gif" class="img-responsive center-block" alt=" "> 
      <h3>Anton Biasizzo</h3>
      <p>Building A, Floor 1, Room 230 <br />
      +386 1 477 3297 <br />
      <a href="mailto:anton.biasizzo@ijs.si">anton.biasizzo@ijs.si</a> <br/>
      <a href="http://cs.ijs.si/biasizzo/biasizzo.html">http://cs.ijs.si/biasizzo/biasizzo.html</a>
      </p>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-md-3">
    <div class="person2">
      <img src="images/staff/jolanda_jakofcic.jpg" class="img-responsive center-block" alt=" "> 
      <h3>Jolanda Jakofčič (Secretary)</h3>
      <p>Building A, Floor 2, Room S8A <br />
      +386 1 477 3582 <br />
      <a href="mailto:jolanda.jakofcic@ijs.si">jolanda.jakofcic@ijs.si</a> <br />
      </p>
    </div>
  </div>
  <div class="clearfix visible-sm-block"></div>
  <div class="col-xs-12 col-sm-6 col-md-3">
    <div class="person2">
      <img src="images/staff/peter_korosec.jpg" class="img-responsive center-block" alt=" "> 
      <h3>Peter Korošec</h3>
      <p>Building A, Floor 2, Room S6F <br />
      +386 1 477 3460 <br />
      <a href="mailto:peter.korosec@ijs.si">peter.korosec@ijs.si</a> <br />
      <a href="http://cs.ijs.si/korosec/">http://cs.ijs.si/korosec/</a> <br />
      His areas of research include combinatorial/numerical optimization with modern metaheuristics and parallel/distributed computing.
      </p>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-md-3">
    <div class="person2">
      <img src="images/staff/barbara_korousic_seljak.jpg" class="img-responsive center-block" alt=" ">
      <h3>Barbara Koroušić Seljak</h3>
      <p>Building A, Floor 2, Room S6D <br />
      +386 1 477 3363 <br />
      <a href="mailto:barbara.korousic@ijs.si">barbara.korousic@ijs.si</a> <br />
      <a href="http://cs.ijs.si/korousic/korousic.html">http://cs.ijs.si/korousic/korousic.html</a>
      </p>
    </div>
  </div>
  </div>
</div>

<div class="section_header">
  <h1><span>Associates</span></h1>
</div>
<div class="container">
  <div class="col-md-4 col-md-offset-2">
    <ul>
      <li>Emil Avdič</li>
      <li>Tome Eftimov</li>
      <li>Gašper Kojek</li>
      <li>Teo Kukuljan</li>
      <li>Simon Mezgec</li>
      <li>Andraž Omahen</li>
      <li>Berislav Pavlovič</li>
      <li>Špela Poklukar</li>
      <li><a href="#person3" data-toggle="collapse">Petra Poklukar</a></li>
      <li>Marina Santo-Zarnik</li>
      <li>Borut Zupančič</li>
    </ul>
  </div>
  <div class="col-md-4">
    <div id="person3" class="collapse person">
      <h3>Petra Poklukar</h3>
    </div>
  </div>
</div>
</main>