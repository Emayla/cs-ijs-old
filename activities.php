<?php
function makeCollapsibleDiv($title, $detail) {
	echo '<div id="' . $detail . '" class="collapse activity">';
	switch ($detail) {
		case 'conferences':
			include('conferences.php');
			break;
		case 'academia':
		case 'industry':
			$today = getdate();
			$meja = $today['year'] - 5;
			$result = mysql_query("SELECT * FROM sodelovanje WHERE (tip=$detail AND leto_zadnje>$meja AND prikaz='1') 
				ORDER BY ustanova ASC");
			echo "<ul>";
			while($row = mysql_fetch_array($result)){
  			echo"<li>";
  			echo "$row[ustanova], $row[mesto], $row[drzava]";
  			echo "</li>";
  		}
			echo "</ul>";
			mysql_close($con);
			break;
		default:
			$filename = "researchareas/" . $detail . ".php";
			if (is_file($filename)) include($filename);
			else echo "File not found.";
			break;
	}
	echo '</div>';
}

function printLinksAndDivs($links) {
	echo '<ul>';
	foreach ($links as $key => $value) {
				echo '<li><a href="#' . $key . '" data-toggle="collapse">'. $value .'</a></li>';
				makeCollapsibleDiv($value, $key);
			}
	echo '</ul>';
}

include('config.php');
?>

<main>
<div class="section_header">
	<h1><span>Activities</span></h1>
</div>
<div class="container">
	<div class="col-md-12">
		<h2>Research areas</h2>
		<?php 
			$researchAreas = array(
				'advsyst' 	=> 'Advanced systems',
				'applcomp' 	=> 'Applied computing',
				'ehealth' 	=> 'eHealth'
			 );
			printLinksAndDivs($researchAreas);
		?>

		<h2>Collaboration</h2>
		<?php
			$collaboration = array(
				'industry' => 'Industry', 
				'academia' => 'Academic'
			);
			printLinksAndDivs($collaboration);
		?>

		<h2>Organizing</h2>
		<?php
			$organizing = array(
				'conferences' => 'Conferences'
			);
			printLinksAndDivs($organizing);
		?>
	</div>
</div>
</main>