<div class="container">
<?php
  include('config.php');

  #delujoči za aktualne
  $result = mysql_query("SELECT * FROM novice WHERE CURRENT_DATE<=datum_konec ORDER BY datum_vpis DESC");
  
  #tako da je vedno vsaj nekaj
  $result = mysql_query("SELECT * FROM novice WHERE ((CURRENT_DATE-180)<=datum_konec OR datum_konec=0) ORDER BY datum_vpis DESC");
  
  $datumdanes = mysql_query("CURRENT_DATE");
  $datumdanes = date("Y-m-d");
  $newscount = 0;
  $maxitems = 4;
  
  while($row = mysql_fetch_array($result)) {
    $newscount++;
    echo '<div class="event">\n';
    $originalDate = $row[datum_vpis];
    $newDate = date("d. M Y", strtotime($originalDate));

    echo '<h3><span class="readmore"><a href="' . $row[povezava] . '">' . $row[naslov] . '</a></span></h3>\n';
    echo '<div class="date"><span>[</span>' . $newDate . '<span>]</span></div>\n';

    $kje = "images/news/newsID";
    $resultFile = glob($kje . $row[novicaID] . ".*");
    $ext = end(explode('.', $resultFile[0]));
    $file_name = $kje . $row[novicaID] . "." . $ext;

    if (file_exists($file_name)) {
      list($width, $height) = getimagesize($file_name);
      if($width > $height) {
        echo "<a class='photo' href='" . $row[povezava] . "'><img src='$file_name' width=120 alt='newsID".$row[novicaID] . "'/></a>\n";
      }
      else {
        echo "<a class='photo' href='" . $row[povezava] . "'><img src='$file_name' height=120 alt='newsID" . $row[novicaID] . "'/></a>\n";
      }
    }

    echo "<p>".$row[opis]."</p>\n";
    echo "</div>\n";

    if ($newscount>=$maxitems) {
      break;
    }
  }
  mysql_close($con);
?>
<!--end news-->
  <div class="col-md-12">
    <h2>News and events<span align=right><a href='?show=news'>show more</a></span></h2>
  </div>
  <div class="col-md-6">
  	<div class="event">
  		<h3>QuaLiFY meeting<span>.</span></h3>
  		<div class="date"><span>[</span>29. Jun 2015<span>]</span></div>
  		<a href=""><img src="images/news/qualify.gif" width="140px" class="img-responsive" alt="QuaLiFY"></a>
	  	<p>From 29th June to 1st July we are organizing a working meeting of QuaLiFY, which is a 2.5 Mio EU-funded project using results from previous EU-funded projects on food composition and intake, and the relationships between who we are (genotype, phenotype) and what we eat (nutritional status).</p>
  	</div>
  </div>

  <div class="col-md-6">
  	<div class="event">
  		<h3>MANTIS - ECSEL project<span>.</span></h3>
  		<div class="date"><span>[</span>06. May 2015<span>]</span></div>
  		<a href=""><img src="images/news/mantis.png" width="140px" class="img-responsive" alt="QuaLiFY"></a>
	  	<p>The overall concept of MANTIS (Cyber Physical System based Proactive Collaborative Maintenance) is to provide a proactive maintenance service platform architecture based on Cyber Physical Systems that allows to estimate future performance, to predict and prevent imminent failures and to schedule proactive maintenance.</p>
  	</div>
  </div>
  
  <div class="clearfix visible-sm-block"></div>
  
  <div class="col-md-6">
  	<div class="event">
  		<h3>MANTIS - ECSEL project<span>.</span></h3>
  		<div class="date"><span>[</span>06. May 2015<span>]</span></div>
  		<a href=""><img src="images/news/mantis.png" width="140px" class="img-responsive" alt="QuaLiFY"></a>
	  	<p>The overall concept of MANTIS (Cyber Physical System based Proactive Collaborative Maintenance) is to provide a proactive maintenance service platform architecture based on Cyber Physical Systems that allows to estimate future performance, to predict and prevent imminent failures and to schedule proactive maintenance.</p>
  	</div>
  </div>

  <div class="col-md-6">
    <div class="event">
      <h3>QuaLiFY meeting<span>.</span></h3>
      <div class="date"><span>[</span>29. Jun 2015<span>]</span></div>
      <a href=""><img src="images/news/qualify.gif" width="140px" class="img-responsive" alt="QuaLiFY"></a>
      <p>From 29th June to 1st July we are organizing a working meeting of QuaLiFY, which is a 2.5 Mio EU-funded project using results from previous EU-funded projects on food composition and intake, and the relationships between who we are (genotype, phenotype) and what we eat (nutritional status).</p>
    </div>
  </div>
</div>