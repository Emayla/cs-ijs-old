<!-- Footer -->
<div id="footerwrap">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <?php echo (!empty($prirejenaNoga) ? $prirejenaNoga : ""); ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8"> <span class="copyright">&copy; 2015 Computer Systems @ Jožef Stefan Institute. Design by <a href="http://www.templategarden.com" rel="nofollow">TemplateGarden</a></span> </div>
      <div class="col-md-4">
        <ul class="list-inline social-buttons">
          <li><a href="#"><i class="fa fa-twitter"></i></a> </li>
          <li><a href="#"><i class="fa fa-facebook"></i></a> </li>
          <li><a href="#"><i class="fa fa-google-plus"></i></a> </li>
          <li><a href="#"><i class="fa fa-linkedin"></i></a> </li>
        </ul>
      </div>
    </div>
  </div>
</div>