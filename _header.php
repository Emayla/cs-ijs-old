<head>
<meta charset="utf-8">

<title><?php echo $naslov;?></title>
<meta name="description" content="<?php echo $description;?>" />
<meta name="keywords" content="<?php echo $keywords;?>" />
<meta name="robots" content="<?php echo $robots;?>">

<link rel="icon" href="<?php echo $potDoLokalnegaKorena;?>images/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="<?php echo $potDoLokalnegaKorena;?>images/favicon.ico" type="image/x-icon"/>

<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css'/>
<!-- Font Awesome  -->
<link href="css/font-awesome.min.css" rel="stylesheet">
<!-- Web Font  -->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="css/style-ines.css" rel="stylesheet" type="text/css" media="all"/>
<script src="js/jquery.min.js"></script>
</head>