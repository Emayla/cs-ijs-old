<!-- Slider -->
<div class="header-banner"> 
  <script src="js/responsiveslides.min.js"></script> 
  <script>
			 $(function () {
			  $("#slider").responsiveSlides({
				auto: true,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				pager: true,
			  });
			 });
			 </script>
  <div class="container">
    <div class="slider">
      <div class="callbacks_container">
        <ul class="rslides" id="slider">
          <li> <img src="images/pocket-size-kitchen-scale.png" alt="">
            <div class="caption">
              <h1>Secure data storage unit<span>.</span></h1>
              <p>A non-volatile solid-state data storage unit with improved life-time, security, reliability, availability, low power consumption and low operating temperature. Target applications include real-time embedded and portable systems operating in hostile environments.</p>
          </li>
          <li> <img src="images/pocket-size-kitchen-scale.png" alt="">
            <div class="caption">
              <h1>Open platform for clinical nutrition<span>.</span></h1>
              <p>Dietary assessment web- and mobile-based application that provides up-to-date data and information about foods as well as user nutritional recommendations according to his/her clinical condition.</p>
          </li>
          <li> <img src="images/pocket-size-kitchen-scale.png" alt="">
            <div class="caption">
              <h1>Pocket-size kitchen scale<span>.</span></h1>
              <p>We developed a pocket-size kitchen scale, with Bluetooth wireless connectivity to automatically upload the weigh value to smartphone or tablet.</p>
          </li>
          <li> <img src="images/pocket-size-kitchen-scale.png" alt="">
            <div class="caption">
              <h1>Production planning and scheduling<span>.</span></h1>
              <p>Optimization of the manufacturing process and creation of an optimized plan and schedule for production of the cooking plates, while considering all constraints.</p>
          </li>
          <li> <img src="images/pocket-size-kitchen-scale.png" alt="">
            <div class="caption">
              <h1>Rapid simulation of temperature<span>.</span></h1>
              <p>Simulation of temperatures inside the refrigerator at different modes of regulation. <br />
              It is integrated with advanced optimization algorithms to automatically find the optimal regulation of the appliance.</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<main>
<!-- Welcome Section -->
<?php include('about.php'); ?>

<!-- News Section-->
<?php include('news_latest.php'); ?>

<!-- Contacts Section -->
<div class="section_header">
  <h1><span>Contacts</span></h1>
</div>
<div class="container">
  <div class="col-md-3 col-md-offset-3">
  <p>Computer Systems <br />
  Jožef Stefan Institute <br />
  Jamova cesta 39 <br />
  SI-1000 Ljubljana <br />
  Slovenia</p>
  </div>
  <div class="col-md-4">
  <p>+386 1 477 3514 (Head of Department) <br />
  +386 1 477 3582 (Secretary) <br />
  +386 1 477 3882 (fax) <br />
  <a href="mailto:cs@ijs.si">cs@ijs.si</a></p>
  </div>
</div>
</main>